"""
All in one build configuration for a development life cycle.

This script allows user to build the project cross-platform based on the
commandline options input in the tasks file. The idea is to mimic how the
vscode's task system works. So the user can have a elegant and good solution for
speed up the inner software development cycle (edit, compile, test).

Author: Pantless Coder (Nghia Lam)
URL: https://codeberg.org/pantlesscoder/painless_project
"""

__doc__ = 'A cli tool for manage and config project build'
__version__ = '0.0.1'
__author__ = 'Pantless Coder (Nghia Lam)'
